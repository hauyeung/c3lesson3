﻿namespace csharp3_lesson3
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeTo2x2GridToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeTo4x4GridToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeTo6x6GridToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.waterLiliesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sunsetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.winterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.imageToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.resetToolStripMenuItem.Text = "Reset";
            this.resetToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeTo2x2GridToolStripMenuItem,
            this.changeTo4x4GridToolStripMenuItem,
            this.changeTo6x6GridToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // changeTo2x2GridToolStripMenuItem
            // 
            this.changeTo2x2GridToolStripMenuItem.Name = "changeTo2x2GridToolStripMenuItem";
            this.changeTo2x2GridToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.changeTo2x2GridToolStripMenuItem.Text = "Change to 2x2 Grid";
            this.changeTo2x2GridToolStripMenuItem.Click += new System.EventHandler(this.changeTo2x2GridToolStripMenuItem_Click);
            // 
            // changeTo4x4GridToolStripMenuItem
            // 
            this.changeTo4x4GridToolStripMenuItem.Name = "changeTo4x4GridToolStripMenuItem";
            this.changeTo4x4GridToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.changeTo4x4GridToolStripMenuItem.Text = "Change to 4x4 Grid";
            this.changeTo4x4GridToolStripMenuItem.Click += new System.EventHandler(this.changeTo4x4GridToolStripMenuItem_Click);
            // 
            // changeTo6x6GridToolStripMenuItem
            // 
            this.changeTo6x6GridToolStripMenuItem.Name = "changeTo6x6GridToolStripMenuItem";
            this.changeTo6x6GridToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.changeTo6x6GridToolStripMenuItem.Text = "Change to 6x6 Grid";
            this.changeTo6x6GridToolStripMenuItem.Click += new System.EventHandler(this.changeTo6x6GridToolStripMenuItem_Click);
            // 
            // imageToolStripMenuItem
            // 
            this.imageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.waterLiliesToolStripMenuItem,
            this.sunsetToolStripMenuItem,
            this.winterToolStripMenuItem});
            this.imageToolStripMenuItem.Name = "imageToolStripMenuItem";
            this.imageToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.imageToolStripMenuItem.Text = "Image";
            // 
            // waterLiliesToolStripMenuItem
            // 
            this.waterLiliesToolStripMenuItem.Name = "waterLiliesToolStripMenuItem";
            this.waterLiliesToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.waterLiliesToolStripMenuItem.Text = "Water Lilies";
            this.waterLiliesToolStripMenuItem.Click += new System.EventHandler(this.waterLiliesToolStripMenuItem_Click);
            // 
            // sunsetToolStripMenuItem
            // 
            this.sunsetToolStripMenuItem.Name = "sunsetToolStripMenuItem";
            this.sunsetToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.sunsetToolStripMenuItem.Text = "Sunset";
            this.sunsetToolStripMenuItem.Click += new System.EventHandler(this.sunsetToolStripMenuItem_Click);
            // 
            // winterToolStripMenuItem
            // 
            this.winterToolStripMenuItem.Name = "winterToolStripMenuItem";
            this.winterToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.winterToolStripMenuItem.Text = "Winter";
            this.winterToolStripMenuItem.Click += new System.EventHandler(this.winterToolStripMenuItem_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 24);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(284, 238);
            this.mainPanel.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Tiles";
            this.Shown += new System.EventHandler(this.MainForm_Shown_1);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MainForm_Paint);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeTo2x2GridToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeTo4x4GridToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeTo6x6GridToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem waterLiliesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sunsetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem winterToolStripMenuItem;
    }
}

